import java.util.Scanner;
//Aluna: Ana Ádila Colares Costa

class Primeira {

    public static void main(String[] args) {
        System.out.println("Hello World\n==================================================");

        Scanner entrada = new Scanner(System.in);
        System.out.println("Oi, qual eh o seu nome?");
        String s = entrada.next();

        System.out.println("Oi " + s + ", tudo bem?");

    }

}