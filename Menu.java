import java.util.Scanner;
//Aluna: Ana Ádila Colares Costa

public class Menu {

    public static void main(String[] args) {

        int opcao;

        do {
        System.out.println("--------------");
        System.out.println("MENU DE OPÇÕES:\n");
        System.out.println("1- Opção 1");
        System.out.println("2- Opção 2");
        System.out.println("3- Sair\n");
        System.out.println("--------------");
        System.out.println("Digite a opção desejada:");


        Scanner menu = new Scanner(System.in);
        opcao = menu.nextInt();

        switch (opcao) {
          case 1: 
            System.out.println("Você escolheu a primeira opção.\n");
            break;
          case 2: 
            System.out.println("Você escolheu a segunda opção.\n");
            break;
          case 3:
            System.out.println("-------------\nO programa foi encerrado.\n");
            break;
          default:
            System.out.println("Opção inválida, escolha novamente uma opção válida.\n");

        }

        } while (opcao != 3);

    }
    

}
